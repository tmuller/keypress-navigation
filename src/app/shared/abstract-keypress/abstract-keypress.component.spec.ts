import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbstractKeypress } from './abstract.keypress';

describe('AbstractKeypressComponent', () => {
  let component: AbstractKeypress;
  let fixture: ComponentFixture<AbstractKeypress>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbstractKeypress ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractKeypress);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
