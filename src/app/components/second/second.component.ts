import { Component } from '@angular/core';
import { AbstractKeypress } from '../../shared/abstract-keypress/abstract.keypress';
import { KeyPressDistributionService } from '../../shared/services/key-press-distribution.service';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.sass']
})
export class SecondComponent extends AbstractKeypress {

  public keyActions: {[key: string]: () => void} = {
    'k--KeyR': () => { console.log('reacting to R'); },
    'k-as-KeyD': () => {},
    'k--F5': () => { console.log('F5 clicked!!!'); }
  };
  constructor(private keyService: KeyPressDistributionService) {
    super(keyService);
  }

  public reactToKeyPress(key: string) {
    console.log('second one, x=', key);
  }
}
