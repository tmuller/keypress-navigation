import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleLandingComponent } from './example-landing.component';

describe('ExampleLandingComponent', () => {
  let component: ExampleLandingComponent;
  let fixture: ComponentFixture<ExampleLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExampleLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
