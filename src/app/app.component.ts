import { Component, HostListener } from '@angular/core';
import { KeyPressDistributionService } from './shared/services/key-press-distribution.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  @HostListener('document:keyup', ['$event'])
  public onKeyUp(eventData: KeyboardEvent) {
    this.keyService.distributeKeyPress(eventData);
  }

  constructor(private keyService: KeyPressDistributionService) {
  }
}
